#include <iostream>
#include "Encoder.hpp"


Encoder::Encoder(unsigned int pinDT_in, unsigned int pinCLK_in, unsigned int pinSW_in, void (*callback_in)(int, void*), void* userData_in):
pinDT(pinDT_in),
pinCLK(pinCLK_in),
pinSW(pinSW_in),
userCallback(callback_in),
userData(userData_in) {

    if (!setupDone) {
        setupDone = true;
        numEncoder++;
        if (gpioInitialise() < 0) throw;

    }

    state = 0b00;
    direction = '\0';
    swState = 0;

    gpioSetMode( pinDT, PI_INPUT);
    gpioSetMode(pinCLK, PI_INPUT);
    gpioSetMode( pinSW, PI_INPUT);

    gpioSetPullUpDown( pinDT, PI_PUD_UP);
    gpioSetPullUpDown(pinCLK, PI_PUD_UP);
    gpioSetPullUpDown( pinSW, PI_PUD_UP);

    gpioSetAlertFuncEx( pinDT, rotCallback, this);
    gpioSetAlertFuncEx(pinCLK, rotCallback, this);
    gpioSetAlertFuncEx( pinSW,  swCallback, this);
};

Encoder::~Encoder() {
    // gpioSetAlertFuncEx( pinDT, 0, NULL);
    // gpioSetAlertFuncEx(pinCLK, 0, NULL);
    // gpioSetAlertFuncEx( pinSW, 0, NULL);
    if (!--numEncoder)
        gpioTerminate();
}

void Encoder::rotCallback(int gpio, int level, uint32_t tick, void *user) {
    ((Encoder*)user)->handleRotCallback(gpio, level);
}

void Encoder::swCallback(int gpio, int level, uint32_t tick, void *user){
    ((Encoder*)user)->handleSWCallback(gpio, level);
}

void Encoder::trigger(int pulse) {
    // std::cout << pulse << std::endl;
    userCallback(pulse, userData);
    // std::cout << std::endl;
}

void Encoder::handleRotCallback(int gpio, int level){
    int newState;
    if (gpio == pinCLK) {
        newState = (state & 0b10) + level;
    } else if (gpio == pinDT) {
        newState = (state & 0b01) + (level << 1);
    } else { 
        std::cout << "Error: bad pin!" << std::endl;
        throw;
    }

    int transState = (state << 2) + newState;
    // std::cout << "state: " << std::bitset<8>(state) << std::endl;
    // std::cout << "newState: " << std::bitset<8>(newState) << std::endl;
    // std::cout << "transState: " << std::bitset<8>(transState) << std::endl;
    switch (transState) {
        case 0b0001:    // Resting position & Turned right 1
                        direction = 'R';
                        break;
        case 0b0010:    // Resting position & Turned left 1
                        direction = 'L';
                        break;
        case 0b0111:    // R1 or L3 position & Turned right 1
                        direction = 'R';
                        break;
        case 0b0100:    // R1 or L3 position & Turned left  1
                        if (direction == 'L')
                            trigger(-1);
                        break;
        case 0b1011:    // R3 or L1 position & Turned left 1
                        direction = 'L';
                        break;
        case 0b1000:    // R3 or L1 position & Turned right 1
                        if (direction == 'R') 
                            trigger(+1);
                        break;
        case 0b1101:    // R2 or L2 position & Turned left 1
                        direction = 'L';
                        break;
        case 0b1110:    // R2 or L2 position & Turned right 1
                        direction = 'R';
                        break;
        case 0b1100:    // R2 or L2 & Skipped an intermediate 01 or 10 state
                        if (direction == 'L') 
                            trigger(-1);
                        else if (direction == 'R')
                            trigger(+1);
                        else
                            std::cout << "Error. bad direction state" << std::endl;
        default:
                        std::cout << "Error. bad transition state" << std::endl;
    }
    state = newState;
}

void Encoder::handleSWCallback(int gpio, int level) {
    // std::cout << "PIN " << gpio << ": " << level << std::endl;
    // add time debounce?
    if (!swState && level) {
        trigger(0);
        swState = 0;
    }
}
