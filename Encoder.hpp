#ifndef ENCODER_H
#define ENCODER_H

#include <pigpio.h>


class Encoder {
    public:
        /**
         * \brief Constructor for an Encoder instance
         * \param	pinDT_in     GPIO numbered pin for DT
         * \param	pinCLK_in    GPIO numbered pin for CLK
         * \param	pinSW_in     GPIO numbered pin for SW
         * \param	callback_in  Callback function (int, void*) to be called on change
         * \param	userData     User data to add to the callback, usually used for instance handling
         * \return	      Encoder Instance
         */
        Encoder(unsigned int pinDT_in, unsigned int pinCLK_in, unsigned int pinSW_in, void (*callback_in)(int, void*), void* userData);
        ~Encoder();

    private:
        inline static bool setupDone = false;
        inline static int numEncoder = 0;
        int pinDT;
        int pinCLK;
        int pinSW;
        
        static void rotCallback(int gpio, int level, uint32_t tick, void *user);
        static void swCallback(int gpio, int level, uint32_t tick, void *user);

        void handleRotCallback(int gpio, int level);
        void handleSWCallback(int gpio, int level);

        void trigger(int pulse);
        void (*userCallback)(int, void*);
        void* userData;

        int state;
        unsigned char direction;
        int swState;
};

#endif //ENCODER_H
